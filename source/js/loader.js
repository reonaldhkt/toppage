// let reoLoader = function(settingArray){

let vwToPixel = (value)=>{
  return $(window).width() / 100 * value; //指定された数にブラウザの表示領域の幅/100をかける
}
let vhToPixel = (value)=>{
  return $(window).height() / 100 * value; //指定された数にブラウザの表示領域の高さ/100をかける
}

let toPixel = (value)=>{
  value = String(value);
  if(value.match(/vw/)) return vwToPixel(Number(value.replace(/vw/, '')));
  if(value.match(/vh/)) return vhToPixel(Number(value.replace(/vh/, '')));
  if(value.match(/px/)) return Number(value.replace(/px/, ''));
  return 0;
}

function loader(settingArray){

  if(settingArray.asyncLoadTemplateFont == true){ //asyncLoadTemplateFontがtrueだったら

    console.log('共通のフォントを非同期で読み込みます')
    //Google FontのCSSファイルの非同期読み込み(headタグにlinkタグを追加) 使用フォント: Lato, MPlus1p, Russo One
    $('head').append('<link href=\'https://fonts.googleapis.com/css?family=Lato\' rel=\'stylesheet\' type=\'text/css\'>');
    $('head').append('<link href=\'https://fonts.googleapis.com/earlyaccess/mplus1p.css\' rel=\'stylesheet\' type=\'text/css\'>');
    $('head').append('<link href=\'https://fonts.googleapis.com/css?family=Russo+One\' rel=\'stylesheet\' type=\'text/css\'>');
    //bodyタグのfont-familyに指定
    $('body').css('font-family', '\'Lato\', \'Mplus 1p\'');

  }

}
