// ############## 関数を定義 ##############

let getScreenCenter = ()=> $(window).scrollTop() + $(window).height() / 2; //表示領域の真ん中のY座標を返す

// ####### 制作物の内容 #######

let productionList = [

];

{ // 同じ物を複数、配列にプッシュ
  let loop = 0;
  let productionGroups = 3;
  let productionsPerGroup = 2;
  while(loop < productionGroups){
    productionList.push(
      {
        className: 'Group' + String(loop),
        displayName: 'グループ ' + String(loop),
        contents: [
        ]
      }
    );
    let loop2 = 0;
    while(loop2 < productionsPerGroup){
      productionList[loop].contents.push(
        {
          className: `iroha${String(loop*10+loop2)}`,
          displayName: 'いろはにほへと',
          image: '/images/productionImage.jpg',
          description: `
            この要素と文章はJavascriptから動的に生成されています。
          `,
          link: 'https://www.example.com/'
        }
      );
      loop2++;
    }
    loop++;
  }
}


// ############## パララックスに関するもの ##############

// パララックス効果をかける対象のリスト
// element: 効果をかける対象の要素
// speed: 通常のスクロール速度と比べてどのくらい速度を変えるかパーセントで指定 (25% = 0.25)
// standard(): 基準点を返す関数

// function test(){
//   console.log(this);
// }

let parallaxUpdateDistance = 0;
let updateParallaxUpdateDistance = ()=> parallaxUpdateDistance = toPixel('100vh');

let parallaxList = [

  { // 0
    getElement: ()=> $('.banner-image_name_forest'),
    speed: 0.8,
    getStandard: ()=> $(window).scrollTop() + toPixel($('.banner-image_name_forest').css('height')) / 2,
  },
  { // 1
    getElement: ()=> $('.banner-image_name_mountain'),
    speed: 0.6,
    getStandard: ()=> $(window).scrollTop() + toPixel($('.banner-image_name_mountain').css('height')) / 2
  },
  { // 2
    getElement: ()=> $('.banner-image_name_logo'),
    speed: 0.8,
    getStandard: ()=> $(window).scrollTop() + toPixel($('.banner-image_name_logo').css('height')) / 2
  },
  { // 3
    getElement: ()=> $('.banner-image_name_base'),
    speed: 0.05,
    getStandard: ()=> $(window).scrollTop() + toPixel($('.banner-image_name_base').css('height')) / 2
  },
  { // 4
    getElement: ()=> $('.banner-image_name_min'),
    speed: 0.05,
    getStandard: ()=> $(window).scrollTop() + toPixel($('.banner-image_name_base').css('height')) / 2
  },
  { // 5
    getElement: ()=> $('.productionImage-image'),
    speed: 0.8,
    getStandard: ()=> getScreenCenter()
  },
  // { // 6
  //   getElement: ()=> $('.productionContent'),
  //   speed: 1.1,
  //   getStandard: ()=> getScreenCenter()
  // }

];

let screenRatio = 1;
let updateScreenRatio = ()=>{ //リサイズされた時に実行されるようにする
  // screenRatioに画面の縦横比を入れて、もしそれが1より大きかったら1に
  // screenRatio = $(window).width() / $(window).height();
  // if(screenRatio > 1) screenRatio = 1;
}
let updateElement = ()=>{
  let loop = 0;
  while(loop < parallaxList.length){
    let data = parallaxList[loop]
    data.element = data.getElement();
    loop++;
  }
}




// ############## パララックス効果に使うTopの値を変更する関数 ##############
let beforeScrollTop = -1;
let parallaxRender = (force)=>{
  // 見えている範囲に画面外が映ってなくて、前の更新の時とスクロール位置が変わっていたら実行(forceがtrueだったら絶対に実行する)(バウンススクロール対策)
  if((beforeScrollTop != $(window).scrollTop() && $(window).scrollTop() >= 0 && $(window).scrollTop() + $(window).height() <= $(document).height()) || force === true){
    let loop = 0; //loopを定義
    while(loop < parallaxList.length){ //リストの長さだけ繰り返す
      let loop2 = 0; // loop2を定義
      while(loop2 < parallaxList[loop].element.length){ //要素の数だけ繰り返し
        // let element = parallaxList[loop].element.eq(loop2); //リストから要素を代入
        // let elementHeight = toPixel(element.css('height')); //要素の高さを入れる
        // let elementBeforeTop = toPixel(element.css('top')); //要素のcssのtopを入れる
        // //要素の左上の絶対Y座標からtopの値を引いて出した、もともとの座標に、要素の高さの半分を足して、中心点の絶対Y座標をだす
        // let elementCenter = (element.offset().top - elementBeforeTop) + elementHeight / 2;
        // //要素の中心点と基準点のY座標の差をだす。　要素の中心点が、表示領域の中心点より下（Y座標が高い）だったら値が＋になる
        // let difference = elementCenter - parallaxList[loop].standard();
        // //要素のTopをいじってパララックスぽっくする
        // if($(window).scrollTop() >= 0) element.css('top', String(difference * (parallaxList[loop].speed - 1)) + 'px'); //適用
        // 下　圧縮後
        let element = parallaxList[loop].element.eq(loop2); //リストから要素を代入
        element.css('top', String(
          ((element.offset().top - toPixel(element.css('top')) + element.height() / 2) - parallaxList[loop].getStandard()) * (parallaxList[loop].speed * screenRatio - 1)
        ) + 'px'); //適用
        loop2++;
      }
      loop++;
    }
  //パララックス処理終わり
  }
  beforeScrollTop = $(window).scrollTop();
  window.requestAnimationFrame(parallaxRender); // 次のサイト描画のタイミングでこの関数を呼び出すよう設定
}






// ####### ホバー系関数 #######
let productionHoverEnter = (element)=>{ //elementにはproductionクラスを持っている要素を指定する
  if(element.hasClass('production')){
    element.addClass('production_status_hover');
    element.find('.productionContent').find('.productionContent-box_color_yellow').addClass('productionContent-box_color_yellow_status_hover');
    element.find('.production-hoverFrame').addClass('production-hoverFrame_status_hover');
    // element.find('.productionImage').find('.productionImage-image').addClass('productionImage-image_status_hover');
  }
}
let productionHoverLeave = (element)=>{ //elementにはproductionクラスを持っている要素を指定する
  if(element.hasClass('production')){
    element.removeClass('production_status_hover');
    element.find('.productionContent').find('.productionContent-box_color_yellow').removeClass('productionContent-box_color_yellow_status_hover');
    element.find('.production-hoverFrame').removeClass('production-hoverFrame_status_hover');
    // element.find('.productionImage').find('.productionImage-image').removeClass('productionImage-image_status_hover');
  }
}


let nearToHover = ()=>{
  let loop = 0;
  while(loop < $('.production').length){ //production要素の数だけループ
    let element = $('.production').eq(loop); //productionクラスを持っているのloop番目の要素を入れる
    let elementCenter = element.offset().top + element.height() / 2;
    let elementRange = Math.abs(element.height() / 2);
    let screenCenter = getScreenCenter();
    let difference = Math.abs(elementCenter - screenCenter);
    if(difference < elementRange && !element.hasClass('production_status_hover')){ //表示領域の真ん中がこの要素の中にあって、ホバー状態じゃなかったら
      productionHoverEnter(element); //この要素をホバー状態に
    }else{ //満たしていなくて
      if(difference >= elementRange &&　element.hasClass('production_status_hover')){ //ホバー状態のクラスがついてたら
        productionHoverLeave(element); //とる
      }
    }
    loop++;
  }
}









// ####### 初期化処理 #######

let init = ()=>{ //初期化関数



  loader({ // 共通部分の読み込み
    'asyncLoadTemplateFont': true
  });

  // ####### リストから制作物の要素を動的に追加
  {
    // let loop = 0;
    // while(loop < productionList.length){
    //   let production = productionList[loop];
    //   $('.productionGroup_name_GROUP').find('.productionGroup-content').append(`
    //     <div class="production production_name_${production.className}">
    //       <div class="productionContent flex">
    //         <div class="productionTextField flex">
    //           <p class="productionName">${production.name}</p>
    //           <p class="productionURL">${production.link}</p>
    //           <div class="productionDescription">
    //             <p>${production.description}</p>
    //           </div>
    //         </div>
    //         <div class="productionJumpButton flex"><a href="${production.link}">GO!</a></div>
    //         <div class="productionContent-box productionContent-box_color_violet flex"></div>
    //         <div class="productionContent-box productionContent-box_color_navy flex"></div>
    //         <div class="productionContent-box productionContent-box_color_skyblue flex"></div>
    //         <div class="productionContent-box productionContent-box_color_yellow flex">
    //           <a class="jumpLink" href="${production.link}">GO!</a>
    //         </div>
    //       </div>
    //       <div class="productionImage">
    //         <div class="productionImage-image"></div>
    //       </div>
    //       <div class="production-hoverFrame"></div>
    //     </div>
    //   `); //htmlタグを追加
    //   // if($(`production_name_${production.className}`).css('background')) console.log('sushi');
    //   $(`.production_name_${production.className}`).fadeIn(250);
    //   $(`.production_name_${production.className}`).find('.productionImage').find('.productionImage-image').css('background', `
    //     url('${production.image}') center/cover
    //   `);
    //   loop++;
    // }
    let loop = 0;
    while(loop < productionList.length){
      $('.mainContents').append(`
        <div class="productionGroup productionGroup_name_${productionList[loop].className}">
          <div class="productionGroup-name flex">
            <p>${productionList[loop].displayName}</p>
          </div>
          <div class="productionGroup-contents"></div>
        </div>
      `);
      let groupContents = productionList[loop].contents;
      // console.log(groupContents);
      let loop2 = 0;
      while(loop2 < groupContents.length){
        let production = groupContents[loop2];
        // console.log(production);
        console.log($(`.productionGroup_name_${productionList[loop].className}`).find('.productionGroup-contents'));
        $(`.productionGroup_name_${productionList[loop].className}`).find('.productionGroup-contents').append(`
          <div class="production production_name_${production.className}">
            <div class="productionContent flex">
              <div class="productionTextField flex">
                <p class="productionName">${production.displayName}</p>
                <p class="productionURL">${production.link}</p>
                <div class="productionDescription">
                  <p>${production.description}</p>
                </div>
              </div>
              <div class="productionJumpButton flex"><a href="${production.link}">GO!</a></div>
              <div class="productionContent-box productionContent-box_color_0 flex"></div>
              <div class="productionContent-box productionContent-box_color_1 flex"></div>
              <div class="productionContent-box productionContent-box_color_2 flex"></div>
              <div class="productionContent-box productionContent-box_color_3 flex">
                <a class="jumpLink" href="${production.link}">GO!</a>
              </div>
            </div>
            <div class="productionImage">
              <div class="productionImage-image"></div>
            </div>
            <div class="production-hoverFrame"></div>
          </div>
        `); //htmlタグを追加
          $(`.production_name_${production.className}`).find('.productionImage').find('.productionImage-image').css('background', `
            url('${production.image}') center/cover
          `);
        loop2++;
      }
      loop++;
      $('.productionGroup').fadeIn(250);
    }
    console.log('制作物部分の要素書き込み完了');
  }



  // ####### cssの読み込み #######
  $('<link>', {
    href: './css/banner.css', rel: 'stylesheet', type: 'text/css'
  }).appendTo('head'); // banner.cssを非同期読み込み
  console.log('banner.cssを読み込みます');



  //#######  イベントの登録 #######
  if (window.ontouchstart !== null) { //タッチデバイスじゃないとき
    $('body').niceScroll(); //NiceScrollを適用
    $('.productionContent, .productionImage, .production-hoverFrame').on('mouseenter', function(){ //ホバーされたら、クラスを付与してアニメーションさせる
      productionHoverEnter($(this).parents('.production'));
    });
    $('.productionContent, .productionImage, .production-hoverFrame').on('mouseleave', function(){ //ホバーから外れたら、クラスを削除してアニメーションさせる
      productionHoverLeave($(this).parents('.production'));
    });
    $('.productionContent-box_color_yellow, .productionImage').on('click', function(){
      //クリックされたら設定されているリンクに飛ぶ
      location.href = $(this).parents('.production').find('.productionContent').find('.productionContent-box_color_yellow').find('.jumpLink').attr('href');
    });
  }else{ //タッチデバイスの時
    $(window).on('scroll', ()=> nearToHover()); //ロード、スクロールされたら擬似ホバー関数を呼び出し
  }

  $(window).on('resize', ()=> {
    updateScreenRatio();
    parallaxRender(true);
  });

  // ####### パララックス描画する関数を呼び出し ########

  // undefinedになるから、先に実行
  updateScreenRatio();
  updateElement();

  parallaxRender();



}



// 関数の呼び出し


$(function(){ //ローどが終わったら
  init(); //初期化
  console.log('Javascriptが最後まで実行された！')
});
